package pl.blachnio.model.price;

import pl.blachnio.model.InquiryParameters;

/**
 * Created by Konrad on 05.09.2016:22:54
 */
//design pattern: builder
public final class PriceCalculationParametersBuilder {
    InquiryParameters inquiryParameters;
    Double appartmentValue;

    private PriceCalculationParametersBuilder() {
    }

    public static PriceCalculationParametersBuilder anOfferCalculationParameters() {
        return new PriceCalculationParametersBuilder();
    }

    public PriceCalculationParametersBuilder withInquiryParameters(InquiryParameters inquiryParameters) {
        this.inquiryParameters = inquiryParameters;
        return this;
    }

    public PriceCalculationParametersBuilder withAppartmentValue(Double appartmentValue) {
        this.appartmentValue = appartmentValue;
        return this;
    }

    public PriceCalculationParameters build() {
        //todo params verification
        return new PriceCalculationParameters(inquiryParameters, appartmentValue);
    }
}
