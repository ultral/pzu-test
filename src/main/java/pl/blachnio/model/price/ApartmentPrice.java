package pl.blachnio.model.price;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Konrad on 07.09.2016:20:37
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApartmentPrice {
    Double apartmentPrice;

    public Double getApartmentPrice() {
        return apartmentPrice;
    }

    public void setApartmentPrice(Double apartmentPrice) {
        this.apartmentPrice = apartmentPrice;
    }

    public static ApartmentPrice withPrice(Double apartmentPrice) {
        ApartmentPrice ap = new ApartmentPrice();
        ap.setApartmentPrice(apartmentPrice);
        return ap;
    }
}
