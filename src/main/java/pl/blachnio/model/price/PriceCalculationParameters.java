package pl.blachnio.model.price;

import pl.blachnio.model.InquiryParameters;

/**
 * Created by Konrad on 05.09.2016:22:52
 */
//design pattern: immutable object
public class PriceCalculationParameters {
    InquiryParameters inquiryParameters;
    Double appartmentValue;

    public PriceCalculationParameters(InquiryParameters inquiryParameters, Double appartmentValue) {
        this.inquiryParameters = inquiryParameters;
        this.appartmentValue = appartmentValue;
    }

    public InquiryParameters getInquiryParameters() {
        return inquiryParameters;
    }

    public Double getAppartmentValue() {
        return appartmentValue;
    }
}
