package pl.blachnio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Konrad on 07.09.2016:22:35
 */
@Entity
public class AverageSquareMeterEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String addressCity;
    private Double averagePrice;
    private Integer quantity;

    protected AverageSquareMeterEntity() {}

    public AverageSquareMeterEntity(String addressCity, Double averagePrice, Integer quantity) {
        this.addressCity = addressCity;
        this.averagePrice = averagePrice;
        this.quantity = quantity;
    }

    public static AverageSquareMeterEntity fromData(Double apartmentPrice, InquiryParameters inquiryParameters){
        AverageSquareMeterEntity entity =
                new AverageSquareMeterEntity(
                        inquiryParameters.getAddressCity(),
                        apartmentPrice/inquiryParameters.getApartmentSize(),
                        1);
        return entity;
    }

    public AverageSquareMeterEntity updateData(Double apartmentPrice, InquiryParameters inquiryParameters){
        averagePrice = ((averagePrice*quantity) + (apartmentPrice/inquiryParameters.getApartmentSize()))/++quantity;
        return this;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public Double getAveragePrice() {
        return averagePrice;
    }



}
