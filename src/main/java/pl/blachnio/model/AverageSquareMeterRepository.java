package pl.blachnio.model;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Konrad on 07.09.2016:22:47
 */
public interface AverageSquareMeterRepository extends CrudRepository<AverageSquareMeterEntity, Long> {
    AverageSquareMeterEntity findByAddressCity(String addressCity);
}
