package pl.blachnio.model.offer;

/**
 * Created by Konrad on 05.09.2016:21:28
 */
public enum OfferType {
    standard,
    komfort,
    premium
}
