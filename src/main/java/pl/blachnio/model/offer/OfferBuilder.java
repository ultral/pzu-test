package pl.blachnio.model.offer;

import javaslang.control.Option;

/**
 * Created by Konrad on 05.09.2016:21:50
 */
//design pattern: builder
public final class OfferBuilder {
    private OfferType offerType;
    private Double minPrice;
    private Double maxPrice;

    //design pattern: null object
    private static final OfferPrices EMPTY_OFFER_PRICES = OfferPricesBuilder
            .anOfferPrices().withMinPrice(0.0).withMaxPrice(0.0).build();


    private OfferBuilder() {
    }

    public static OfferBuilder anOffer() {
        return new OfferBuilder();
    }

    public OfferBuilder withOfferType(OfferType offerType) {
        this.offerType = offerType;
        return this;
    }

    public OfferBuilder withMinPrice(Double minPrice) {
        this.minPrice = minPrice;
        return this;
    }

    public OfferBuilder withMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
        return this;
    }

    public OfferBuilder withOfferPrices(OfferPrices offerPrices) {
        this.minPrice = offerPrices.getMinPrice();
        this.maxPrice = offerPrices.getMaxPrice();
        return this;
    }

    public OfferBuilder withOfferPrices(Option<OfferPrices> offerPricesOption) {
        return withOfferPrices(offerPricesOption.getOrElse(EMPTY_OFFER_PRICES));
    }

    public Offer build() {
        //todo params verification
        return new Offer(offerType, minPrice, maxPrice);
    }



}
