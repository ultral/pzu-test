package pl.blachnio.model.offer;

/**
 * Created by Konrad on 06.09.2016:18:33
 */
//design pattern: builder
public final class OfferPricesBuilder {
    private Double minPrice;
    private Double maxPrice;

    private OfferPricesBuilder() {
    }

    public static OfferPricesBuilder anOfferPrices() {
        return new OfferPricesBuilder();
    }

    public OfferPricesBuilder withMinPrice(Double minPrice) {
        this.minPrice = minPrice;
        return this;
    }

    public OfferPricesBuilder withMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
        return this;
    }

    public OfferPrices build() {
        //todo params verification
        return new OfferPrices(minPrice, maxPrice);
    }
}
