package pl.blachnio.model.offer;

/**
 * Created by Konrad on 06.09.2016:18:32
 */
//design pattern: immutable object
public class OfferPrices {
    private Double maxPrice;
    private Double minPrice;

    public OfferPrices(Double minPrice, Double maxPrice) {
        this.maxPrice = maxPrice;
        this.minPrice = minPrice;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }
}
