package pl.blachnio.model.offer;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Konrad on 05.09.2016:21:27
 */
//design pattern: immutable object
public class Offer {
    private OfferType offerType;
    private Double minPrice;
    private Double maxPrice;

    public Offer(OfferType offerType, Double minPrice, Double maxPrice) {
        this.offerType = offerType;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
    }

    @JsonProperty(required = true)
    public OfferType getOfferType() {
        return offerType;
    }

    @JsonProperty(required = true)
    public Double getMinPrice() {
        return minPrice;
    }

    @JsonProperty(required = true)
    public Double getMaxPrice() {
        return maxPrice;
    }
}
