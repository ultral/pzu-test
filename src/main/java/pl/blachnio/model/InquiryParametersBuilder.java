package pl.blachnio.model;

import java.util.Date;

/**
 * Created by Konrad on 08.09.2016:00:19
 */
//design pattern: builder
public final class InquiryParametersBuilder {
    private Double apartmentSize;
    private Date insuranceStartDate;
    private String addressCity;

    private InquiryParametersBuilder() {
    }

    public static InquiryParametersBuilder anInquiryParameters() {
        return new InquiryParametersBuilder();
    }

    public InquiryParametersBuilder withApartmentSize(Double apartmentSize) {
        this.apartmentSize = apartmentSize;
        return this;
    }

    public InquiryParametersBuilder withInsuranceStartDate(Date insuranceStartDate) {
        this.insuranceStartDate = insuranceStartDate;
        return this;
    }

    public InquiryParametersBuilder withAddressCity(String addressCity) {
        this.addressCity = addressCity;
        return this;
    }

    public InquiryParameters build() {
        return new InquiryParameters(apartmentSize, insuranceStartDate, addressCity);
    }
}
