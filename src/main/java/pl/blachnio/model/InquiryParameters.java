package pl.blachnio.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Konrad on 05.09.2016:21:35
 */
//design pattern: immutable object
public class InquiryParameters {
    private Double apartmentSize;
    private Date insuranceStartDate;
    private String addressCity;

    InquiryParameters(Double apartmentSize, Date insuranceStartDate, String addressCity) {
        this.apartmentSize = apartmentSize;
        this.insuranceStartDate = insuranceStartDate;
        this.addressCity = addressCity;
    }

    @JsonProperty(required = true)
    @ApiModelProperty(notes = "Size of apartment in square meters. Example: 65.5", required = true)
    public Double getApartmentSize() {
        return apartmentSize;
    }

    @JsonProperty(required = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(notes = "Start date of insurance in format: yyyy-MM-dd. Example: 2016-04-21", required = true)
    public Date getInsuranceStartDate() {
        return insuranceStartDate;
    }

    @JsonProperty(required = true)
    @ApiModelProperty(notes = "City where apartment is. Example: Warsaw", required = true)
    public String getAddressCity() {
        return addressCity;
    }
}
