package pl.blachnio;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.blachnio.model.InquiryParametersBuilder;
import pl.blachnio.model.offer.Offer;
import pl.blachnio.services.OfferCalculatorService;

import java.util.Date;
import java.util.List;

/**
 * Created by Konrad on 07.09.2016:22:29
 */
@SuppressWarnings("unused")
@RestController
public class CalculationController {
    @Autowired
    private OfferCalculatorService offerCalculatorService;

    @ApiOperation(value = "calculateOffer", nickname = "calculateOffer")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "addressCity", value = "City where apartment is. Example: Warsaw", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "apartmentSize", value = "Size of apartment in square meters. Example: 65.5", required = true, dataType = "double", paramType = "query"),
            @ApiImplicitParam(name = "insuranceStartDate", value = "Start date of insurance in format: yyyy-MM-dd. Example: 2016-04-21", required = true, dataType = "date", paramType = "query")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Offer.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @SuppressWarnings("unused")
    @RequestMapping(path = "/calculateOffer", method = RequestMethod.GET, produces = "application/json")
    List<Offer> calculateOffer(@RequestParam(value="apartmentSize") Double apartmentSize,
                               @RequestParam(value="addressCity") String addressCity,
                               @RequestParam(value="insuranceStartDate")
                               @DateTimeFormat(pattern = "yyyy-MM-dd") Date insuranceStartDate) {
        return offerCalculatorService
                .calculateOffer(InquiryParametersBuilder
                        .anInquiryParameters()
                        .withAddressCity(addressCity)
                        .withApartmentSize(apartmentSize)
                        .withInsuranceStartDate(insuranceStartDate)
                        .build())
                .toJavaList();
    }
}
