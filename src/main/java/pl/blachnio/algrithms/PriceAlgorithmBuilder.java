package pl.blachnio.algrithms;

import javaslang.collection.List;
import pl.blachnio.algrithms.calculations.PriceCalculation;
import pl.blachnio.algrithms.conditions.Condition;

/**
 * Created by Konrad on 06.09.2016:20:57
 */
//design pattern: builder
public final class PriceAlgorithmBuilder {
    List<Condition> conditionList;
    List<PriceCalculation> maxPriceCalculationList;
    List<PriceCalculation> minPriceCalculationList;

    private PriceAlgorithmBuilder() {
    }

    public static PriceAlgorithmBuilder aPriceAlgorithm() {
        return new PriceAlgorithmBuilder();
    }


    public PriceAlgorithmBuilder withConditions(Condition... conditions) {
        this.conditionList = List.of(conditions);
        return this;
    }

    public PriceAlgorithmBuilder withConditions(List<Condition> conditionList) {
        this.conditionList = conditionList;
        return this;
    }

    public PriceAlgorithmBuilder withMaxPriceCalculations(PriceCalculation... maxPriceCalculations) {
        this.maxPriceCalculationList = List.of(maxPriceCalculations);
        return this;
    }

    public PriceAlgorithmBuilder withMinPriceCalculations(PriceCalculation... minPriceCalculations) {
        this.minPriceCalculationList = List.of(minPriceCalculations);
        return this;
    }

    public PriceAlgorithmBuilder withMinPriceCalculations(List<PriceCalculation> minPriceCalculations) {
        this.minPriceCalculationList = minPriceCalculations;
        return this;
    }

    public PriceAlgorithm build() {
        //todo params verification
        return new PriceAlgorithm(conditionList, maxPriceCalculationList, minPriceCalculationList);
    }
}
