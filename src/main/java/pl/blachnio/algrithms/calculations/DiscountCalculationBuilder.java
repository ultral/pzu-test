package pl.blachnio.algrithms.calculations;

import javaslang.control.Option;

/**
 * Created by Konrad on 06.09.2016:21:48
 */
//design pattern: builder
public final class DiscountCalculationBuilder {
    Double percent = null;
    Double constant = null;

    private DiscountCalculationBuilder() {
    }

    public static DiscountCalculationBuilder aDiscountCalculation() {
        return new DiscountCalculationBuilder();
    }

    public DiscountCalculationBuilder withPercent(Double percent) {
        this.percent = percent;
        return this;
    }

    public DiscountCalculationBuilder withConstant(Double constant) {
        this.constant = constant;
        return this;
    }

    public DiscountCalculation build() {
        //todo params verification
        return new DiscountCalculation(Option.of(percent), Option.of(constant));
    }
}
