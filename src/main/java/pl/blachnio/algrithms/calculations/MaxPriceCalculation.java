package pl.blachnio.algrithms.calculations;

import javaslang.control.Option;

/**
 * Created by Konrad on 06.09.2016:21:30
 */
//design pattern: immutable object
public class MaxPriceCalculation implements PriceCalculation {
    Option<Double> promil;
    Option<Double> constant;

    public MaxPriceCalculation(Option<Double> promil, Option<Double> constant) {
        this.promil = promil;
        this.constant = constant;
    }

    @Override
    public Double calculate(Double price) {
        if (!promil.isEmpty()) {
            price = price / 1000 * promil.get();
        }
        if (!constant.isEmpty()) {
            price = price + constant.get();
        }
        return price;
    }
}
