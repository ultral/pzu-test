package pl.blachnio.algrithms.calculations;

/**
 * Created by Konrad on 06.09.2016:20:13
 */
public interface PriceCalculation {
    Double calculate(Double price);
}
