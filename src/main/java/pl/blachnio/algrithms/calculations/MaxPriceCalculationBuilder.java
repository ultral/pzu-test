package pl.blachnio.algrithms.calculations;

import javaslang.control.Option;

/**
 * Created by Konrad on 06.09.2016:21:35
 */
//design pattern: builder
public final class MaxPriceCalculationBuilder {
    Double promil = null;
    Double constant = null;

    private MaxPriceCalculationBuilder() {
    }

    public static MaxPriceCalculationBuilder aMaxPriceCalculation() {
        return new MaxPriceCalculationBuilder();
    }

    public MaxPriceCalculationBuilder withPromil(Double promil) {
        this.promil = promil;
        return this;
    }

    public MaxPriceCalculationBuilder withConstant(Double constant) {
        this.constant = constant;
        return this;
    }


    public MaxPriceCalculation build() {
        //todo params verification
        return new MaxPriceCalculation(Option.of(promil), Option.of(constant));
    }
}
