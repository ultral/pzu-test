package pl.blachnio.algrithms.calculations;

/**
 * Created by Konrad on 06.09.2016:21:54
 */
//design pattern: builder
public final class LowestPriceCalculationBuilder {
    Double lowestPrice;

    private LowestPriceCalculationBuilder() {
    }

    public static LowestPriceCalculationBuilder aLowestPriceCalculation() {
        return new LowestPriceCalculationBuilder();
    }

    public LowestPriceCalculationBuilder withLowestPrice(Double lowestPrice) {
        this.lowestPrice = lowestPrice;
        return this;
    }

    public LowestPriceCalculation build() {
        //todo params verification
        return new LowestPriceCalculation(lowestPrice);
    }
}
