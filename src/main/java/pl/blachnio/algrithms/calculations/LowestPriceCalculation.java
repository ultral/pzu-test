package pl.blachnio.algrithms.calculations;

/**
 * Created by Konrad on 06.09.2016:21:53
 */
//design pattern: immutable object
public class LowestPriceCalculation implements PriceCalculation {
    Double lowestPrice;

    public LowestPriceCalculation(Double lowestPrice) {
        this.lowestPrice = lowestPrice;
    }

    @Override
    public Double calculate(Double price) {
        return price < lowestPrice ? lowestPrice : price;
    }
}
