package pl.blachnio.algrithms.calculations;

import javaslang.control.Option;

/**
 * Created by Konrad on 06.09.2016:21:44
 */
//design pattern: immutable object
public class DiscountCalculation implements PriceCalculation {
    Option<Double> percent;
    Option<Double> constant;

    public DiscountCalculation(Option<Double> percent, Option<Double> constant) {
        this.percent = percent;
        this.constant = constant;
    }

    @Override
    public Double calculate(Double price) {
        if (!percent.isEmpty()) {
            price = price / 100 * (100 - percent.get());
        }
        if (!constant.isEmpty()) {
            price = price - constant.get();
        }
        return price;
    }
}
