package pl.blachnio.algrithms.conditions;

import javaslang.control.Option;
import pl.blachnio.model.price.PriceCalculationParameters;


/**
 * Created by Konrad on 06.09.2016:21:15
 */
//design pattern: immutable object
public class ApartmentValueCondition implements Condition {
    Option<Double> minValue;
    Option<Double> maxValue;

    public ApartmentValueCondition(Option<Double> minValue, Option<Double> maxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @Override
    public boolean fulfills(PriceCalculationParameters priceCalculationParameters) {
        if (minValue.isEmpty()) {
            return priceCalculationParameters.getAppartmentValue() <= maxValue.get();
        }
        if (maxValue.isEmpty()) {
            return priceCalculationParameters.getAppartmentValue() >= minValue.get();
        }

        return priceCalculationParameters.getAppartmentValue() <= maxValue.get()
                && priceCalculationParameters.getAppartmentValue() >= minValue.get();
    }
}
