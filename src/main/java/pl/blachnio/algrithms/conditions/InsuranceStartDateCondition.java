package pl.blachnio.algrithms.conditions;

import javaslang.control.Option;
import pl.blachnio.model.price.PriceCalculationParameters;

import java.util.Date;

/**
 * Created by Konrad on 06.09.2016:21:02
 */
//design pattern: immutable object
public class InsuranceStartDateCondition implements Condition {
    Option<Date> minDate;
    Option<Date> maxDate;

    public InsuranceStartDateCondition(Option<Date> minDate, Option<Date> maxDate) {
        this.minDate = minDate;
        this.maxDate = maxDate;
    }

    @Override
    public boolean fulfills(PriceCalculationParameters priceCalculationParameters) {
        if (minDate.isEmpty()) {
            return priceCalculationParameters.getInquiryParameters().getInsuranceStartDate().before(maxDate.get());
        }
        if (maxDate.isEmpty()) {
            return priceCalculationParameters.getInquiryParameters().getInsuranceStartDate().after(minDate.get());
        }

        return priceCalculationParameters.getInquiryParameters().getInsuranceStartDate().before(maxDate.get())
                && priceCalculationParameters.getInquiryParameters().getInsuranceStartDate().after(minDate.get());
    }
}
