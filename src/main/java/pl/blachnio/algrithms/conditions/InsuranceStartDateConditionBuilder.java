package pl.blachnio.algrithms.conditions;

import javaslang.control.Option;

import java.util.Date;

/**
 * Created by Konrad on 06.09.2016:21:09
 */
//design pattern: builder
public final class InsuranceStartDateConditionBuilder {
    Date minDate = null;
    Date maxDate = null;

    private InsuranceStartDateConditionBuilder() {
    }

    public static InsuranceStartDateConditionBuilder anInsuranceStartDateCondition() {
        return new InsuranceStartDateConditionBuilder();
    }

    public InsuranceStartDateConditionBuilder withMinDate(Date minDate) {
        this.minDate = minDate;
        return this;
    }

    public InsuranceStartDateConditionBuilder withMaxDate(Date maxDate) {
        this.maxDate = maxDate;
        return this;
    }

    public InsuranceStartDateCondition build() {
        //todo test if mindate < maxdate
        return new InsuranceStartDateCondition(Option.of(minDate), Option.of(maxDate));
    }
}
