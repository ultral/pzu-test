package pl.blachnio.algrithms.conditions;

import javaslang.control.Option;

/**
 * Created by Konrad on 06.09.2016:21:22
 */
//design pattern: builder
public final class ApartmentValueConditionBuilder {
    Double minValue = null;
    Double maxValue = null;

    private ApartmentValueConditionBuilder() {
    }

    public static ApartmentValueConditionBuilder anApartmentValueCondition() {
        return new ApartmentValueConditionBuilder();
    }

    public ApartmentValueConditionBuilder withMinValue(Double minValue) {
        this.minValue = minValue;
        return this;
    }

    public ApartmentValueConditionBuilder withMaxValue(Double maxValue) {
        this.maxValue = maxValue;
        return this;
    }

    public ApartmentValueCondition build() {
        //todo params verification
        return new ApartmentValueCondition(Option.of(minValue), Option.of(maxValue));
    }
}
