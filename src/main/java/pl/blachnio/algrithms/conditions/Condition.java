package pl.blachnio.algrithms.conditions;

import pl.blachnio.model.price.PriceCalculationParameters;

/**
 * Created by Konrad on 06.09.2016:20:02
 */
public interface Condition {
    boolean fulfills(PriceCalculationParameters priceCalculationParameters);
}
