package pl.blachnio.algrithms;

import javaslang.collection.List;
import pl.blachnio.algrithms.calculations.PriceCalculation;
import pl.blachnio.algrithms.conditions.Condition;
import pl.blachnio.model.offer.OfferPricesBuilder;
import pl.blachnio.model.price.PriceCalculationParameters;
import pl.blachnio.model.offer.OfferPrices;

/**
 * Created by Konrad on 05.09.2016:23:59
 */
//design pattern: immutable object
public class PriceAlgorithm {

    //design pattern: chain of command
    private List<Condition> conditionList;
    //design pattern: chain of command
    private List<PriceCalculation> maxPriceCalculationList;
    //design pattern: chain of command
    private List<PriceCalculation> minPriceCalculationList;

    public PriceAlgorithm(List<Condition> conditionList,
                          List<PriceCalculation> maxPriceCalculationList,
                          List<PriceCalculation> minPriceCalculationList) {
        this.conditionList = conditionList;
        this.maxPriceCalculationList = maxPriceCalculationList;
        this.minPriceCalculationList = minPriceCalculationList;
    }

    public boolean isApplicable(PriceCalculationParameters priceCalculationParameters) {
        return conditionList.find(condition -> !condition.fulfills(priceCalculationParameters)).isEmpty();
    }

    public OfferPrices calculatePrices(PriceCalculationParameters priceCalculationParameters) {
        Double maxPrice = applyPriceCalculations(priceCalculationParameters.getAppartmentValue(), maxPriceCalculationList);
        Double minPrice = applyPriceCalculations(maxPrice, minPriceCalculationList);

        return OfferPricesBuilder.anOfferPrices().withMinPrice(minPrice).withMaxPrice(maxPrice).build();
    }

    //todo can it by done functionally?
    private Double applyPriceCalculations(Double initialValue, List<PriceCalculation> priceCalculationList) {
        for (PriceCalculation priceCalculation : priceCalculationList) {
            initialValue = priceCalculation.calculate(initialValue);
        }
        return initialValue;
    }
}
