package pl.blachnio.mock;

import org.springframework.web.bind.annotation.*;
import pl.blachnio.model.price.ApartmentPrice;

import java.util.Random;

/**
 * Created by Konrad on 07.09.2016:20:32
 */
@RestController
public class ExternalApartmentServiceMock {

    private final Random r = new Random();

    @RequestMapping(path = "/apartmentValue", method = RequestMethod.GET)
    ApartmentPrice calculateOffer(@RequestParam(value="apartmentSize") Double apartmentSize,
                                  @RequestParam(value="addressCity") String addressCity) {
        return ApartmentPrice.withPrice(apartmentSize*(r.nextInt(12000)+2000));
    }
}
