package pl.blachnio.configuration;

import javaslang.collection.HashMap;
import javaslang.collection.List;
import javaslang.collection.Map;
import pl.blachnio.algrithms.PriceAlgorithm;
import pl.blachnio.algrithms.PriceAlgorithmBuilder;
import pl.blachnio.algrithms.calculations.DiscountCalculationBuilder;
import pl.blachnio.algrithms.calculations.LowestPriceCalculationBuilder;
import pl.blachnio.algrithms.calculations.MaxPriceCalculationBuilder;
import pl.blachnio.algrithms.calculations.PriceCalculation;
import pl.blachnio.algrithms.conditions.ApartmentValueConditionBuilder;
import pl.blachnio.algrithms.conditions.Condition;
import pl.blachnio.algrithms.conditions.InsuranceStartDateConditionBuilder;
import pl.blachnio.model.offer.OfferType;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by Konrad on 06.09.2016:22:39
 */
public class OfferConfiguration {

    //todo change name of params
    private static PriceCalculation lowestPrice76 = LowestPriceCalculationBuilder.aLowestPriceCalculation().withLowestPrice(76.0).build();
    private static PriceCalculation lowestPrice96 = LowestPriceCalculationBuilder.aLowestPriceCalculation().withLowestPrice(96.0).build();
    private static PriceCalculation lowestPrice107 = LowestPriceCalculationBuilder.aLowestPriceCalculation().withLowestPrice(107.0).build();
    private static PriceCalculation discount20 = DiscountCalculationBuilder.aDiscountCalculation().withConstant(20.0).build();
    private static Condition standardApartmentValueCondition = ApartmentValueConditionBuilder.anApartmentValueCondition().withMinValue(50000.0).withMaxValue(1000000.0).build();
    private static Condition insuranceStartDateMax2016_09_30 = InsuranceStartDateConditionBuilder.anInsuranceStartDateCondition()
            .withMaxDate(Date.from(LocalDate.of(2016, Month.SEPTEMBER, 30).atStartOfDay(ZoneId.systemDefault()).toInstant())).build();
    private static Condition insuranceStartDateMin2016_10_01 = InsuranceStartDateConditionBuilder.anInsuranceStartDateCondition()
            .withMinDate(Date.from(LocalDate.of(2016, Month.OCTOBER, 1).atStartOfDay(ZoneId.systemDefault()).toInstant())).build();
    private static List<Condition> firstOfferStandardConditions = List.of(standardApartmentValueCondition, insuranceStartDateMax2016_09_30);
    private static List<Condition> secondOfferStandardConditions = List.of(standardApartmentValueCondition, insuranceStartDateMin2016_10_01);
    private static List<PriceCalculation> secondOfferMinPriceCalculations = List.of(discount20, lowestPrice76);

    private static final Map<OfferType, List<PriceAlgorithm>> STANDARD_OFFER_CONFIGURATION = HashMap.of(OfferType.standard,
                List.of(
                        PriceAlgorithmBuilder.aPriceAlgorithm()
                                .withConditions(firstOfferStandardConditions)
                                .withMaxPriceCalculations(
                                        MaxPriceCalculationBuilder.aMaxPriceCalculation().withPromil(1.0).build(),
                                        lowestPrice76)
                                .withMinPriceCalculations(
                                        DiscountCalculationBuilder.aDiscountCalculation().withPercent(6.0).build(),
                                        lowestPrice76)
                                .build(),
                        PriceAlgorithmBuilder.aPriceAlgorithm()
                                .withConditions(secondOfferStandardConditions)
                                .withMaxPriceCalculations(
                                        MaxPriceCalculationBuilder.aMaxPriceCalculation().withPromil(1.1).build(),
                                        lowestPrice76)
                                .withMinPriceCalculations(secondOfferMinPriceCalculations)
                                .build()
                )
        );

    private static final Map<OfferType, List<PriceAlgorithm>> KOMFORT_OFFER_CONFIGURATION = HashMap.of(OfferType.komfort,
                    List.of(
                            PriceAlgorithmBuilder.aPriceAlgorithm()
                                    .withConditions(firstOfferStandardConditions)
                                    .withMaxPriceCalculations(
                                            MaxPriceCalculationBuilder.aMaxPriceCalculation().withPromil(1.2).build(),
                                            lowestPrice96)
                                    .withMinPriceCalculations(
                                            DiscountCalculationBuilder.aDiscountCalculation().withPercent(8.0).build(),
                                            lowestPrice96)
                                    .build(),
                            PriceAlgorithmBuilder.aPriceAlgorithm()
                                    .withConditions(secondOfferStandardConditions)
                                    .withMaxPriceCalculations(
                                            MaxPriceCalculationBuilder.aMaxPriceCalculation().withPromil(1.3).build(),
                                            lowestPrice96)
                                    .withMinPriceCalculations(secondOfferMinPriceCalculations)
                                    .build()
                    )
            );

    private static final Map<OfferType, List<PriceAlgorithm>> PREMIUM_OFFER_CONFIGURATION = HashMap.of(OfferType.premium,
                List.of(
                        PriceAlgorithmBuilder.aPriceAlgorithm()
                                .withConditions(firstOfferStandardConditions)
                                .withMaxPriceCalculations(
                                        MaxPriceCalculationBuilder.aMaxPriceCalculation().withPromil(1.2).withConstant(13.0).build(),
                                        lowestPrice107)
                                .withMinPriceCalculations(
                                        DiscountCalculationBuilder.aDiscountCalculation().withPercent(3.0).build(),
                                        lowestPrice107)
                                .build(),
                        PriceAlgorithmBuilder.aPriceAlgorithm()
                                .withConditions(secondOfferStandardConditions)
                                .withMaxPriceCalculations(
                                        MaxPriceCalculationBuilder.aMaxPriceCalculation().withPromil(1.3).withConstant(13.0).build(),
                                        lowestPrice107)
                                .withMinPriceCalculations(secondOfferMinPriceCalculations)
                                .build()
                )
        );


    private static final Map<OfferType, List<PriceAlgorithm>> ALL_OFFERS_CONFIGURATION;
    static {
        Map<OfferType, List<PriceAlgorithm>> offersConfiguration = HashMap.empty();
        offersConfiguration = offersConfiguration.put(STANDARD_OFFER_CONFIGURATION.get());
        offersConfiguration = offersConfiguration.put(KOMFORT_OFFER_CONFIGURATION.get());
        offersConfiguration = offersConfiguration.put(PREMIUM_OFFER_CONFIGURATION.get());
        ALL_OFFERS_CONFIGURATION = offersConfiguration;
    }

    public static Map<OfferType, List<PriceAlgorithm>> provideConfiguration() {
        return ALL_OFFERS_CONFIGURATION;
    }
}
