package pl.blachnio.services;

import javaslang.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import pl.blachnio.model.AverageSquareMeterEntity;
import pl.blachnio.model.AverageSquareMeterRepository;
import pl.blachnio.model.price.ApartmentPrice;
import pl.blachnio.model.InquiryParameters;

import javax.annotation.PostConstruct;
import java.net.SocketTimeoutException;

/**
 * Created by Konrad on 05.09.2016:22:28
 */
@Service
class ApartmentPriceService {

    private Logger LOGGER = LoggerFactory.getLogger(ApartmentPriceService.class);

    //todo read from props
    private int timeout = 10000;
    //todo read from props
    private String externalApartmentPriceServiceLocation = "http://kbpzu.herokuapp.com/apartmentValue?apartmentSize={1}&addressCity={2}";
    private Double defSquareMeterValue = 3000.0;

    private SimpleClientHttpRequestFactory requestFactory;
    private RestTemplate restTemplate;

    @Autowired
    private AverageSquareMeterRepository repository;

    @PostConstruct
    private void init() {
        //todo is this thread safe?
        requestFactory = new SimpleClientHttpRequestFactory(){{
                    setConnectTimeout(timeout);
                    setReadTimeout(timeout);
        }};

        restTemplate = new RestTemplate(){{setRequestFactory(requestFactory);}};
    }

    Double calculatePrice(InquiryParameters inquiryParameters) {
        //design pattern: compose method (NOT composite method)
        return getApartmentPriceFromExteralService(inquiryParameters)
                .getOrElse(getEstimatedApartmentPrice(inquiryParameters));
    }

    private double getEstimatedApartmentPrice(InquiryParameters inquiryParameters) {
        return inquiryParameters.getApartmentSize()*getSquareMeterValue(inquiryParameters);
    }

    private Option<Double> getApartmentPriceFromExteralService(InquiryParameters inquiryParameters) {
        try {
            ResponseEntity<ApartmentPrice> resp = getStringResponseEntity(inquiryParameters);
            if (resp.getStatusCode() == HttpStatus.OK) {
                Double apartmentPrice = resp.getBody().getApartmentPrice();
                putValueIntoRepo(apartmentPrice, inquiryParameters);
                return Option.of(apartmentPrice);
            }
        } catch (HttpClientErrorException | ResourceAccessException e) {
            LOGGER.info("External Apartment Price Service at: " + externalApartmentPriceServiceLocation + " is not reachable.", e);
        }
        return Option.none();
    }

    private ResponseEntity<ApartmentPrice> getStringResponseEntity(InquiryParameters inquiryParameters) throws ResourceAccessException {
        return restTemplate.getForEntity(
                externalApartmentPriceServiceLocation,
                ApartmentPrice.class,
                inquiryParameters.getApartmentSize(),
                inquiryParameters.getAddressCity());
    }

    private Double getSquareMeterValue(InquiryParameters inquiryParameters) {
        AverageSquareMeterEntity entity = repository.findByAddressCity(inquiryParameters.getAddressCity());
        return entity == null ? defSquareMeterValue : entity.getAveragePrice();
    }

    private void putValueIntoRepo(Double apartmentPrice, InquiryParameters inquiryParameters) {
        AverageSquareMeterEntity entity = repository.findByAddressCity(inquiryParameters.getAddressCity());
        if (entity == null) {
            repository.save(AverageSquareMeterEntity.fromData(apartmentPrice, inquiryParameters));
        } else {
            repository.save(entity.updateData(apartmentPrice, inquiryParameters));
        }
    }
}
