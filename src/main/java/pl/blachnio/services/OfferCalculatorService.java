package pl.blachnio.services;

import javaslang.collection.List;
import javaslang.collection.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.blachnio.algrithms.PriceAlgorithm;
import pl.blachnio.configuration.OfferConfiguration;
import pl.blachnio.model.InquiryParameters;
import pl.blachnio.model.offer.Offer;
import pl.blachnio.model.offer.OfferBuilder;
import pl.blachnio.model.offer.OfferType;
import pl.blachnio.model.price.PriceCalculationParameters;
import pl.blachnio.model.price.PriceCalculationParametersBuilder;


/**
 * Created by Konrad on 05.09.2016:21:40
 */
@Service
public class OfferCalculatorService {

    @Autowired
    ApartmentPriceService apartmentPriceService;

    //design pattern: chain of command
    private static final Map<OfferType, List<PriceAlgorithm>> OFFER_CALCULATOR_CONFIGURATION_MAP = OfferConfiguration.provideConfiguration();

    public List<Offer> calculateOffer(InquiryParameters inquiryParameters) {
        PriceCalculationParameters params =
                PriceCalculationParametersBuilder
                        .anOfferCalculationParameters()
                        .withInquiryParameters(inquiryParameters)
                        .withAppartmentValue(apartmentPriceService.calculatePrice(inquiryParameters))
                        .build();

        return OFFER_CALCULATOR_CONFIGURATION_MAP
                .mapValues(
                        priceAlgorithmList -> priceAlgorithmList
                                .find(priceAlgorithm -> priceAlgorithm.isApplicable(params))
                                .map(priceAlgorithm -> priceAlgorithm.calculatePrices(params)))
                .traverse(
                        (offerType, offerPrices) -> OfferBuilder.anOffer()
                                .withOfferType(offerType)
                                .withOfferPrices(offerPrices)
                                .build())
                .toList();
    }
}
